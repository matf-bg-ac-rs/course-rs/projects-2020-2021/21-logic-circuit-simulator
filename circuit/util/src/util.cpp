#include <util.hpp>
#include <cstdlib>
#include <iostream>

void util::greska(std::string s){
    std::cerr << "ERROR: " << s << "!" << std::endl;
    std::exit(EXIT_FAILURE);
}

void util::upozorenje(std::string s){
    std::cerr << "WARNING: " << s << "!" << std::endl;
}
